import base from './base'
import wepy from 'wepy';

export default class api extends base {

	static postUserService(data){
		return this.post(this.baseUrl + "/api/service/add", data)
	}

	static getUserServicelist(data){
		return this.post(this.baseUrl + "/api/service/lists", data)
	}

	static getContentList(data){
		return this.get(this.baseUrl + '/api/content/lists', data);
	}

	static getContentDetail(data){
		return this.get(this.baseUrl + '/api/content/detail', data);
	}

	static wxJsCode2Session(data){
		return this.post(this.baseUrl + '/api/wechat/jscode2session', data);
	}
}